using System;
using UnityEngine;

namespace VoxelGame
{
	[Serializable]
	public class BulletPatternEntry : AttackEntry
	{
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Transform spawner;

		public virtual Bullet[] Shoot ()
		{
			Do ();
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
	}
}