using System;
using UnityEngine;

namespace VoxelGame
{
	[Serializable]
	public class AttackEntry
	{
		public string name;
		public AnimationEntry animationEntry;
		
		public virtual void Do ()
		{
			animationEntry.Play();
		}
	}
}