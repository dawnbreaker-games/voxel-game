using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class BidirectionalMoveableAutoResizableList3D<T> : BidirectionalMoveableAutoResizableList<BidirectionalMoveableAutoResizableList2D<T>>
{
	public new T this[int xIndex, int yIndex, int zIndex]
	{
		get
		{
			return this[xIndex][yIndex, zIndex];
		}
		set
		{
			if (autoResizeWhenGetAndSetElements)
			{
				if (xIndex - indexRange.min >= values.Count)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList2D<T>(), true);
				}
				else if (xIndex - indexRange.min < 0)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList2D<T>(), false);
				}
			}
			this[xIndex][yIndex, zIndex] = value;
		}
	}

	public BidirectionalMoveableAutoResizableList3D () : base ()
	{
	}

	public BidirectionalMoveableAutoResizableList3D (Vector3Int minIndex, Vector3Int maxIndex, bool autoResizeWhenGetAndSetElements = true)
	{
		indexRange = new IntRange(minIndex.x, maxIndex.x);
		this.autoResizeWhenGetAndSetElements = autoResizeWhenGetAndSetElements;
		for (int x = minIndex.x; x <= maxIndex.x; x ++)
		{
			for (int y = minIndex.y; y <= maxIndex.y; y ++)
			{
				for (int z = minIndex.z; z <= maxIndex.z; z ++)
					this[x] = new BidirectionalMoveableAutoResizableList2D<T>(new Vector2Int(minIndex.y, maxIndex.y), new Vector2Int(minIndex.z, maxIndex.z));
					// Insert (new Vector3Int(x, y, z), default(T), x > maxIndex.x, y > maxIndex.y, z > maxIndex.z);
			}
		}
	}

	public void Insert (Vector3Int index, T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		BidirectionalMoveableAutoResizableList2D<T> list = this[index.x];
		list.Insert(new Vector2Int(index.y, index.z), element, toYEnd, toZEnd);
		if (Count > 1)
		{
			if (toXEnd)
				indexRange.max ++;
			else
				indexRange.min --;
		}
	}

	public bool Remove (T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		for (int i = indexRange.min; i <= indexRange.max; i ++)
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[i];
			if (list.Remove(element, toYEnd, toZEnd))
			{
				if (toXEnd)
					indexRange.max --;
				else
					indexRange.min ++;
				return true;
			}
		}
		return false;
	}

	public void RemoveAt (Vector3Int index, bool toXEnd, bool toYEnd)
	{
		BidirectionalMoveableAutoResizableList2D<T> list = this[index.x];
		list.RemoveAt(index.x, toYEnd);
		if (toXEnd)
			indexRange.max --;
		else
			indexRange.min ++;
	}

	public void Add (T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		if (toXEnd)
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[Count - 1];
			Add(list);
			if (Count > 1)
				indexRange.max ++;
		}
		else
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[0];
			list.Insert(Vector2Int.zero, element, toYEnd, toZEnd);
			Insert(0, list);
			if (Count > 1)
				indexRange.min --;
		}
	}
}