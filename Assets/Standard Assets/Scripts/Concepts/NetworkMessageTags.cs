namespace VoxelGame
{
	public struct NetworkMessageTags
	{
		public const byte SPAWN_PLAYER = 0;
		public const byte SYNC_TRANSFORM_MOVED = 1;
		public const byte SYNC_TRANSFORM_ROTATED = 2;
		public const byte PLAYER_DIED = 3;
		public const byte PLAYER_LEFT = 4;
		public const byte PLAYER_STARTED_ACTION = 5;
	}
}