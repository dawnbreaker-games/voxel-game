﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	[CreateAssetMenu]
	public class SpawnAtPlayer : BulletPattern
	{
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			return new Bullet[] { Instantiate(bulletPrefab, Player.Instance.trs.position, spawner.rotation) };
		}
	}
}