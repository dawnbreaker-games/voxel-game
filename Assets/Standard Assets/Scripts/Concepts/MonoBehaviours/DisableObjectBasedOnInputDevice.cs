﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	public class DisableObjectBasedOnInputDevice : MonoBehaviour
	{
		public bool disableIfUsing;
		public InputManager.InputDevice inputDevice;
		
		void Start ()
		{
			gameObject.SetActive((InputManager.Instance.inputDevice == inputDevice || (InputManager.UsingGamepad && inputDevice == InputManager.InputDevice.Gamepad)) != disableIfUsing);
		}
	}
}