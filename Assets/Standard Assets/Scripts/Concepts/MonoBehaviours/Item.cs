using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	public class Item : MonoBehaviour
	{
		public Transform trs;

		public virtual void OnGain ()
		{
		}
		
		public virtual void Equip ()
		{
			gameObject.SetActive(true);
		}

		public virtual void Unequip ()
		{
			gameObject.SetActive(false);
		}
	}
}