﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	public class Hazard : Spawnable
	{
		public float damage;
		public Renderer renderer;
		public float radius;
		public bool damageOnCollision;
		public bool damageOnEnter;
		public static List<Hazard> instances = new List<Hazard>();

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Remove(this);
		}
		
		public virtual void OnCollisionEnter (Collision coll)
		{
			if (!damageOnCollision)
				return;
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage);
		}
		
		public virtual void OnTriggerEnter (Collider other)
		{
			if (!damageOnEnter)
				return;
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage);
		}
	}
}