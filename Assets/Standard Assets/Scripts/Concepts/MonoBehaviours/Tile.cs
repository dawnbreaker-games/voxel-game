using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace VoxelGame
{
	public class Tile : MonoBehaviour, IDestructable
	{
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return MaxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public MeshRenderer meshRenderer;
		public Tile[] neighbors = new Tile[0];
		public Tile[] supportingTiles = new Tile[0];
		public bool isSupportingTile;
		public TileParent tileParent;
		float hp;
		bool dead;

		public virtual void OnEnable ()
		{
		}

		public virtual void OnDestroy ()
		{
			if (GameManager.paused || _SceneManager.isLoading || GameManager.isQuitting)
				return;
			for (int i = 0; i < neighbors.Length; i ++)
			{
				Tile tile = neighbors[i];
				tile.neighbors = tile.neighbors.Remove(this);
			}
			if (tileParent != null)
			{
				// tileParent.UpdateRigidbody ();
				tileParent.rigid.ResetCenterOfMass();
				tileParent.rigid.ResetInertiaTensor();
				tileParent.rigid.mass -= meshRenderer.bounds.GetVolume();
				ICollisionEnterHandler collisionEnterHandler = this as ICollisionEnterHandler;
				if (collisionEnterHandler != null)
					tileParent.collisionEnterHandlers = tileParent.collisionEnterHandlers.Remove(collisionEnterHandler);
			}
			for (int i = 0; i < neighbors.Length; i ++)
			{
				Tile neighbor = neighbors[i];
				Tile[] connectedTiles = Tile.GetConnectedGroup(neighbor);
				bool shouldSplit = true;
				for (int i2 = 0; i2 < neighbor.supportingTiles.Length; i2 ++)
				{
					Tile supportingTile = neighbor.supportingTiles[i2];
					if (connectedTiles.Contains(supportingTile) && supportingTile != null)
					{
						shouldSplit = false;
						break;
					}
				}
				if (shouldSplit)
					SplitTiles (connectedTiles);
			}
		}

		public virtual void TakeDamage (float amount)
		{
			hp -= amount;
			if (hp <- 0)
			{
				if (!dead)
					Death ();
			}
		}


		public virtual void Death ()
		{
			dead = true;
			Destroy(gameObject);
		}

		public static void SplitTiles (Tile[] tiles)
		{
			TileParent tileParent = Instantiate(GameManager.instance.tileParentPrefab);
			tileParent.name += TileParent.lastUniqueId;
			TileParent.lastUniqueId ++;
			tileParent.rigid.mass = 0;
			for (int i = 0; i < tiles.Length; i ++)
			{
				Tile tile = tiles[i];
				tile.trs.SetParent(tileParent.trs);
				if (tile.tileParent != null && tile.tileParent.trs.childCount == 0)
					Destroy(tile.tileParent.gameObject);
				tile.tileParent = tileParent;
				tileParent.rigid.mass += tile.meshRenderer.bounds.GetVolume();
			}
			// tileParent.UpdateRigidbody ();
			tileParent.rigid.ResetCenterOfMass();
			tileParent.rigid.ResetInertiaTensor();
			tileParent.collisionEnterHandlers = tileParent.GetComponentsInChildren<ICollisionEnterHandler>();
		}

		public static bool AreNeighbors (Tile tile, Tile tile2, BoundsOffset? tileBoundsOffset = null, BoundsOffset? tile2BoundsOffset = null)
		{
			if (tileBoundsOffset == null)
				tileBoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			if (tile2BoundsOffset == null)
				tile2BoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			Vector3[] pointsInsideTile = tile.meshRenderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tileBoundsOffset);
			Vector3[] pointsInsideTile2 = tile2.meshRenderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tile2BoundsOffset);
			for (int i = 0; i < pointsInsideTile.Length; i ++)
			{
				Vector3 pointInsideTile = pointsInsideTile[i];
				pointInsideTile = pointInsideTile.Snap(Vector3.one / 2);
				for (int i2 = 0; i2 < pointsInsideTile2.Length; i2 ++)
				{
					Vector3 pointInsideTile2 = pointsInsideTile2[i2];
					pointInsideTile2 = pointInsideTile2.Snap(Vector3.one / 2);
					if ((pointInsideTile - pointInsideTile2).sqrMagnitude <= 1)
						return true;
				}
			}
			return false;
		}

		public static bool AreOverlapping (Tile tile, Tile tile2, BoundsOffset? tileBoundsOffset = null, BoundsOffset? tile2BoundsOffset = null)
		{
			if (tileBoundsOffset == null)
				tileBoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			if (tile2BoundsOffset == null)
				tile2BoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			Vector3[] pointsInsideTile = tile.meshRenderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tileBoundsOffset);
			Vector3[] pointsInsideTile2 = tile2.meshRenderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tile2BoundsOffset);
			for (int i = 0; i < pointsInsideTile.Length; i ++)
			{
				Vector3 pointInsideTile = pointsInsideTile[i];
				pointInsideTile = pointInsideTile.Snap(Vector3.one / 2);
				for (int i2 = 0; i2 < pointsInsideTile2.Length; i2 ++)
				{
					Vector3 pointInsideTile2 = pointsInsideTile2[i2];
					pointInsideTile2 = pointInsideTile2.Snap(Vector3.one / 2);
					if ((pointInsideTile - pointInsideTile2).sqrMagnitude <= .1f)
						return true;
				}
			}
			return false;
		}

		public static Tile[] GetConnectedGroup (Tile tile)
		{
			List<Tile> output = new List<Tile>();
			List<Tile> checkedTiles = new List<Tile>() { tile };
			List<Tile> remainingTiles = new List<Tile>() { tile };
			while (remainingTiles.Count > 0)
			{
				Tile tile2 = remainingTiles[0];
				output.Add(tile2);
				for (int i = 0; i < tile2.neighbors.Length; i ++)
				{
					Tile connectedTile = tile2.neighbors[i];
					if (!checkedTiles.Contains(connectedTile))
					{
						checkedTiles.Add(connectedTile);
						remainingTiles.Add(connectedTile);
					}
				}
				remainingTiles.RemoveAt(0);
			}
			return output.ToArray();
		}
	}
}