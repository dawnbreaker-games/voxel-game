namespace VoxelGame
{
	public class BombItem : UsableItem
	{
		public override void Use ()
		{
			base.Use ();
			for (int i = 0; i < Bullet.instances.Count; i ++)
			{
				Bullet bullet = Bullet.instances[i];
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				i --;
			}
		}
	}
}