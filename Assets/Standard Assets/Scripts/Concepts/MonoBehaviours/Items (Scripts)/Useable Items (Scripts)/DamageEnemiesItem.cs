using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace VoxelGame
{
	public class DamageEnemiesItem : UsableItem
	{
		public float range;
		public float damage;

		public override void Use ()
		{
			base.Use ();
			SpatialHashGrid2D<Entity>.Agent[] agents = NetworkManager.entitySpatialHashGrid.GetClosebyAgents(trs.position.GetXZ());
			for (int i = 0; i < agents.Length; i ++)
			{
				SpatialHashGrid2D<Entity>.Agent agent = agents[i];
				Enemy enemy = agent.value as Enemy;
				if (enemy != null && (trs.position - enemy.trs.position).sqrMagnitude <= range)
					enemy.TakeDamage (damage);
			}
		}
	}
}