using UnityEngine;

namespace VoxelGame
{
	public class UsableWeapon : UsableItem
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;
		public bool autoShoot;
		public Collider collider;

		public override void OnGain ()
		{
			trs.SetParent(Player.instance.headTrs);
			if (collider != null)
			{
				Physics.IgnoreCollision(collider, Player.instance.controller);
				Physics.IgnoreCollision(collider, Player.instance.collider);
			}
			base.OnGain ();
			if (!autoShoot)
				Player.instance.bulletPatternEntriesSortedList.Add(bulletPatternEntry.name, bulletPatternEntry);
		}

		public override void Use ()
		{
            base.Use ();
			if (autoShoot)
	            bulletPatternEntry.Shoot ();
			animationEntry.Play ();
		}

		void OnDestroy ()
		{
			if (Player.instance == null)
				return;
			Player.instance.bulletPatternEntriesSortedList.Remove(bulletPatternEntry.name);
			animationEntry.animator.Play("None", animationEntry.layer);
		}
	}
}