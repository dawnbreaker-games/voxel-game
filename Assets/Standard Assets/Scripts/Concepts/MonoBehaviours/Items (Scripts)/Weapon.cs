using UnityEngine;

namespace VoxelGame
{
	public class Weapon : Item
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;

		public override void OnGain ()
		{
			bulletPatternEntry.spawner = Player.instance.bulletSpawnersParent;
			animationEntry.animator = Player.instance.animator;
			Player.instance.bulletPatternEntriesSortedList[bulletPatternEntry.name] = bulletPatternEntry;
			animationEntry.Play ();
		}

		void OnDestroy ()
		{
			if (Player.instance == null)
				return;
			Player.instance.bulletPatternEntriesSortedList.Remove(bulletPatternEntry.name);
			animationEntry.animator.Play("None", animationEntry.layer);
		}
	}
}