using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;

namespace VoxelGame
{
	public class UsableItem : Item
	{
		public InputActionReference useAction;
		public float cooldown;
		[HideInInspector]
		public float lastUseTime;
		public bool canHoldUseButton;
		static UseUpdater useUpdater;

		public override void OnGain ()
		{
			useAction.action.Enable();
		}

		public void TryToUse (InputAction.CallbackContext context = default(InputAction.CallbackContext))
		{
			if (!GameManager.paused && gameObject.activeInHierarchy && Time.time - lastUseTime >= cooldown)
				Use ();
			if (canHoldUseButton && useUpdater == null)
			{
				useUpdater = new UseUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(useUpdater);
			}
		}

		public virtual void Use ()
		{
			lastUseTime = Time.time;
		}

		public virtual void OnDisable ()
		{
			useAction.action.Disable();
		}

		class UseUpdater : IUpdatable
		{
			UsableItem usableItem;

			public UseUpdater (UsableItem usableItem)
			{
				this.usableItem = usableItem;
			}

			public void DoUpdate ()
			{
				if (usableItem.useAction.action.ReadValue<float>() == 1)
					usableItem.TryToUse ();
				else
				{
					useUpdater = null;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}