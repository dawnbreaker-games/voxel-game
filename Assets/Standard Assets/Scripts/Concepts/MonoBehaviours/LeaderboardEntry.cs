using UnityEngine;

namespace VoxelGame
{
	public class LeaderboardEntry : MonoBehaviour
	{
		public _Text rankText;
		public _Text valueText;
		public _Text usernameText;
		public GameObject[] valueTypeIndicators = new GameObject[0];

		public enum ValueType
		{
			Time,
			TotalTime,
			Tasks
		}
	}
}