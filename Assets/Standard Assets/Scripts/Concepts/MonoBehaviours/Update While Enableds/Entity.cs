using Extensions;
using UnityEngine;
using Pathfinding;
using System.Collections.Generic;

namespace VoxelGame
{
	[ExecuteInEditMode]
	public class Entity : UpdateWhileEnabled, IDestructable, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Animator animator;
		public Transform trs;
		public Transform headTrs;
		public Rigidbody rigid;
		public CharacterController controller;
		public Collider collider; 
		public float moveSpeed;
		public float jumpSpeed;
		public float maxJumpDuration;
		public float groundCheckDistance;
		public LayerMask whatICollideWith;
		public Transform groundCheckPoint;
		public float slideRate;
		public Seeker seeker;
		public float targetDistanceFromPathPoints;
		public SyncTransform syncTrs;
		public float radius;
		public SerializableDictionary<string, AttackEntry> attackEntriesDict = new SerializableDictionary<string, AttackEntry>();
		public float attackRange;
		protected Vector3 move;
		protected bool canJump;
		protected bool isGrounded;
		protected float timeLastGrounded;
		protected float yVel;
		protected Vector3 currentPathPoint;
		protected bool isDead;
		List<Vector3> pathPoints = new List<Vector3>();
		float targetDistanceFromPathPointsSqr;
		Vector2 previousPositionXZ;

		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (controller == null)
					controller = GetComponent<CharacterController>();
				if (groundCheckPoint == null)
					groundCheckPoint = trs.Find("Ground Check Point");
				if (seeker == null)
					seeker = GetComponent<Seeker>();
				return;
			}
#endif
			targetDistanceFromPathPointsSqr = targetDistanceFromPathPoints * targetDistanceFromPathPoints;
			// NetworkManager.syncTransformsDict.Add(syncTrs.id, syncTrs);
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			hp = maxHp; 
			if (syncTrs != null)
				syncTrs.enabled = true;
			base.OnEnable ();
		}

		void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			SpatialHashGrid2D<Entity>.Agent agent = new SpatialHashGrid2D<Entity>.Agent(trs.position.GetXZ(), this, NetworkManager.entitySpatialHashGrid);
			NetworkManager.entitySpatialHashGridAgentsDict.Add(this, agent);
		}
		
		public override void DoUpdate ()
		{
			move = Vector3.zero;
			isGrounded = controller.isGrounded;
			if (isGrounded)
			{
				timeLastGrounded = Time.time;
				canJump = true;
				yVel = 0;
			}
			HandleRotating ();
			HandleMoving ();
			HandleGravity ();
			HandleJump ();
			if (controller.enabled)
				controller.Move(move * Time.deltaTime);
			Vector2 agentPosition = trs.position.XZToXY();
			if (agentPosition != previousPositionXZ)
			{
				previousPositionXZ = agentPosition;
				NetworkManager.entitySpatialHashGridAgentsDict[this].Update (previousPositionXZ);
			}
		}

		public virtual void HandlePathfinding ()
		{
			if (pathPoints.Count > 1 && (trs.position - currentPathPoint).sqrMagnitude <= targetDistanceFromPathPointsSqr)
			{
				pathPoints.RemoveAt(0);
				currentPathPoint = pathPoints[0];
			}
		}

		public void SetDestination (Vector3 position)
		{
			seeker.CancelCurrentPathRequest();
			seeker.StartPath(trs.position, position, OnPathCalculated);
		}

		void OnPathCalculated (Path path)
		{
			pathPoints = path.vectorPath;
			currentPathPoint = pathPoints[0];
		}

		public virtual void HandleRotating ()
		{
			if (pathPoints.Count > 0)
				trs.forward = (currentPathPoint - trs.position).GetXZ();
		}
		
		public virtual void HandleMoving ()
		{
			if (pathPoints.Count > 0)
				move = (currentPathPoint - trs.position).normalized;
			if (controller.enabled)
			{
				rigid.isKinematic = true;
				move *= moveSpeed;
			}
			else
			{
				rigid.velocity = (move * moveSpeed).SetY(rigid.velocity.y);
				rigid.isKinematic = false;
			}
		}
		
		public virtual void HandleGravity ()
		{
			if (controller.enabled && !isGrounded)
			{
				yVel += Physics.gravity.y * Time.deltaTime;
				move += Vector3.up * yVel;
			}
		}
		
		public virtual void HandleJump ()
		{
		}
		
		protected void Jump ()
		{
			if (controller.enabled)
			{
				yVel += jumpSpeed * Time.deltaTime;
				move += Vector3.up * yVel;
			}
		}

		void OnCollisionEnter (Collision coll)
		{
			HandleSlopes ();
		}

		void OnCollisionStay (Collision coll)
		{
			HandleSlopes ();
		}

		void HandleSlopes ()
		{
			RaycastHit hit;
			if (Physics.Raycast(groundCheckPoint.position, Vector3.down, out hit, groundCheckDistance, whatICollideWith))
			{
				float slopeAngle = Vector3.Angle(hit.normal, Vector3.up);
				if (slopeAngle <= controller.slopeLimit)
				{
					controller.enabled = true;
					rigid.velocity = Vector2.zero;
				}
				else
				{
					if (controller.enabled)
						rigid.velocity = controller.velocity;
					controller.enabled = false;
					rigid.velocity += Vector3.down * Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * slideRate * Time.deltaTime;
				}
			}
		}

		public virtual void TakeDamage (float damage)
		{
			hp -= damage;
			if (hp <= 0)
				Death ();
		}

		public virtual void Death ()
		{
			if (isDead)
				return;
			isDead = true;
			NetworkManager.entitySpatialHashGridAgentsDict[this].Remove ();
			Destroy(gameObject);
		}

		public void SetFacing (Vector3 newFacing)
		{
			trs.forward = newFacing.SetY(0);
			headTrs.forward = newFacing;
		}
	}
}