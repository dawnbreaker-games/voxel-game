using Extensions;
using UnityEngine;
using Pathfinding;
using System.Collections.Generic;

namespace VoxelGame
{
	public class Enemy : Entity
	{
		public float targetDistanceFromPlayer;
		public Transform eyesTrs;
		public float visionRange;
		public float visionDegrees;
		public float visionSphereCastRadius;
		public LayerMask whatBlocksVision;
		public float minPlayerPositionChangeToRepath;
		public float difficulty;
		public float interestRange;
		float minPlayerPositionChangeToRepathSqr;
		float visionRangeSqr;
		Vector3 lastKnownPlayerPosition;
		float targetDistanceFromPlayerSqr;
		Player targetPlayer;
		bool canSeeTargetPlayer;
		float distanceToTargetPlayerSqr;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (eyesTrs == null)
					eyesTrs = trs.Find("Eyes");
				return;
			}
#endif
			visionRangeSqr = visionRange * visionRange;
			targetDistanceFromPlayerSqr = targetDistanceFromPlayer * targetDistanceFromPlayer;
			minPlayerPositionChangeToRepathSqr = minPlayerPositionChangeToRepath * minPlayerPositionChangeToRepath;
		}

		public override void OnDisable ()
		{
		}

		public override void DoUpdate ()
		{
			if (targetPlayer == null)
				enabled = false;
			HandleSetTargetPlayer ();
			if (targetPlayer != null)
			{
				canSeeTargetPlayer = CanSeePlayer(targetPlayer);
				HandlePathfinding ();
				if (canSeeTargetPlayer)
					HandleAttacking ();
			}
			base.DoUpdate ();
		}

		void HandleSetTargetPlayer ()
		{
			distanceToTargetPlayerSqr = Mathf.Infinity;
			Player[] closebyPlayers = GetClosebyPlayers();
			for (int i = 0; i < closebyPlayers.Length; i ++)
			{
				Player player = closebyPlayers[i];
				if (CanSeePlayer(player))
				{
					SetTargetPlayerIfShould (player);
					enabled = true;
				}
			}
		}

		public override void HandlePathfinding ()
		{
			if (canSeeTargetPlayer)
			{
				Vector3 playerPosition = targetPlayer.trs.position;
				if ((lastKnownPlayerPosition - playerPosition).sqrMagnitude >= minPlayerPositionChangeToRepathSqr)
				{
					SetDestination (playerPosition);
					lastKnownPlayerPosition = playerPosition;
				}
			}
			base.HandlePathfinding ();
		}
		
		public override void HandleRotating ()
		{
			if (canSeeTargetPlayer)
				trs.forward = (targetPlayer.trs.position - trs.position).GetXZ();
			else
				base.HandleRotating ();
		}
		
		public override void HandleMoving ()
		{
			if (canSeeTargetPlayer && distanceToTargetPlayerSqr < targetDistanceFromPlayerSqr)
				move = (trs.position - targetPlayer.trs.position).normalized;
			else
				base.HandleMoving ();
		}

		public override void Death ()
		{
			if (!isDead)
			{
				NetworkManager.npcSyncTransformsDict.Remove(syncTrs.id);
				NetworkManager.syncTransformsDict.Remove(syncTrs.id);
				GameManager.updatables = GameManager.updatables.Remove(this);
				base.Death ();
			}
		}

		public virtual void HandleAttacking ()
		{
			if (distanceToTargetPlayerSqr <= attackRange * attackRange)
				Attack ();
		}

		public virtual void Attack ()
		{
			attackEntriesDict.values[0].Do ();
		}

		bool SetTargetPlayerIfShould (Player player)
		{
			float distanceToPlayerSqr = (trs.position - player.trs.position).sqrMagnitude;
			if (targetPlayer == null || distanceToTargetPlayerSqr > distanceToPlayerSqr)
			{
				distanceToTargetPlayerSqr = distanceToPlayerSqr;
				targetPlayer = player;
				return true;
			}
			else
				return false;
		}

		bool CanSeePlayer (Player player)
		{
			Vector3 toPlayer = player.trs.position - eyesTrs.position;
			if (toPlayer.sqrMagnitude <= visionRangeSqr && Vector3.Angle(eyesTrs.forward, toPlayer) <= visionDegrees / 2)
			{
				RaycastHit hit;
				if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, toPlayer, out hit, visionRange, whatBlocksVision))
				{
					if (hit.collider.GetComponentInParent<Player>(true) == player)
						return true;
				}
			}
			return false;
		}
		
		Player[] GetClosebyPlayers ()
		{
			List<Player> output = new List<Player>();
			SpatialHashGrid2D<Entity>.Agent[] entitySpatialHashGridAgents = NetworkManager.entitySpatialHashGrid.GetClosebyAgents(trs.position.GetXZ());
			for (int i = 0; i < entitySpatialHashGridAgents.Length; i ++)
			{
				SpatialHashGrid2D<Entity>.Agent entitySpatialHashGridAgent = entitySpatialHashGridAgents[i];
				Player player = entitySpatialHashGridAgent.value as Player;
				if (player != null)
					output.Add(player);
			}
			return output.ToArray();
		}

		Player GetClosestPlayer ()
		{
			SpatialHashGrid2D<Entity>.Agent[] entitySpatialHashGridAgents = NetworkManager.entitySpatialHashGrid.GetClosebyAgents(trs.position.GetXZ());
			Player closestPlayer = null;
			float closestDistanceSqr = Mathf.Infinity;
			for (int i = 1; i < entitySpatialHashGridAgents.Length; i ++)
			{
				SpatialHashGrid2D<Entity>.Agent entitySpatialHashGridAgent = entitySpatialHashGridAgents[i];
				Player player = entitySpatialHashGridAgent.value as Player;
				if (player != null)
				{
					float distanceSqr = (trs.position - player.trs.position).sqrMagnitude;
					if (distanceSqr < closestDistanceSqr)
					{
						closestPlayer = player;
						closestDistanceSqr = distanceSqr;
					}
				}
			}
			return closestPlayer;
		}
	}
}