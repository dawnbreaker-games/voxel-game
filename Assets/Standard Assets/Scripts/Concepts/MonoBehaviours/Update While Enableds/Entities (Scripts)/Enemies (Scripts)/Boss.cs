using UnityEngine;

namespace VoxelGame
{
	public class Boss : Enemy
	{
		public static Boss instance;
		public static Boss Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Boss>();
				return instance;
			}
		}

		public override void TakeDamage (float amount)
		{
			float previousHp = Hp;
			base.TakeDamage (amount);
			BossLevel bossLevel = (BossLevel) Level.instance;
			bossLevel.totalDamage += previousHp - Hp;
			bossLevel.currentTimeText.Text = "Damage: " + string.Format("{0:0.#}", bossLevel.totalDamage);
			if (bossLevel.totalDamage >= bossLevel.totalMaxHp)
				bossLevel.End ();
		}
	}
}