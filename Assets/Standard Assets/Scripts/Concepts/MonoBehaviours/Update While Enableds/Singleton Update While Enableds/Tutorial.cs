using UnityEngine;
using UnityEngine.Playables;

namespace VoxelGame
{
	public class Tutorial : SingletonUpdateWhileEnabled<Tutorial>
	{
		public PlayableDirector playableDirector;
		public GameObject[] activateOnFinish = new GameObject[0];

		public override void OnEnable ()
		{
			base.OnEnable ();
			if (!SaveAndLoadManager.saveData.showTutorials)
			{
				GameManager.instance.temporaryActiveTextNotification.text.Text = "";
				Destroy(gameObject);
			}
		}

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				GameObject go = activateOnFinish[i];
				go.SetActive(true);
			}
		}
	}
}