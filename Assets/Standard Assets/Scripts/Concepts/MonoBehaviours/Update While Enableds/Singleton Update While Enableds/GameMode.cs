using DarkRift;
using Extensions;
using UnityEngine;
using DarkRift.Client;

namespace VoxelGame
{
	public class GameMode : SingletonUpdateWhileEnabled<GameMode>
	{
		public Player playerPrefab;
		public SpawnPoint[] spawnPoints = new SpawnPoint[0];
		public float respawnDelay;

		public class RespawnUpdater : IUpdatable
		{
			Player player;
			float timeRemaining;

			public RespawnUpdater (Player player)
			{
				this.player = player;
				timeRemaining = GameMode.instance.respawnDelay;
			}

			public void DoUpdate ()
			{
				timeRemaining -= Time.deltaTime;
				if (timeRemaining <= 0)
				{
					SpawnPoint spawnPoint = instance.spawnPoints[Random.Range(0, instance.spawnPoints.Length)];
					NetworkManager.instance.SpawnPlayer (spawnPoint.trs.position, spawnPoint.trs.rotation, player.Id);
            		using (DarkRiftWriter writer = DarkRiftWriter.Create())
					{
						writer.Write(player.Id);
						writer.Write(spawnPoint.trs.position.x);
						writer.Write(spawnPoint.trs.position.y);
						writer.Write(spawnPoint.trs.position.z);
						writer.Write(spawnPoint.trs.eulerAngles.x);
						writer.Write(spawnPoint.trs.eulerAngles.y);
						writer.Write(spawnPoint.trs.eulerAngles.z);
						Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer);
						NetworkManager.instance.darkRift2Client.SendMessage(message, SendMode.Reliable);
					}
					Destroy(player.gameObject);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}