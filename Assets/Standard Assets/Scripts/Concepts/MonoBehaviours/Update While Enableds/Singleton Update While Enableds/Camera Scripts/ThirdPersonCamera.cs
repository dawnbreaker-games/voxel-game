using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace VoxelGame
{
	public class ThirdPersonCamera : CameraScript
	{
		public float rotateRate;
		float cameraDistance;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			cameraDistance = trs.localPosition.magnitude;
			trs.SetParent(null);
			base.Awake ();
		}

		public override void HandlePosition ()
		{
			trs.position = Player.instance.trs.position + -trs.forward * cameraDistance;
		}
		
		public override void HandleRotation ()
		{
			Vector2 rotaInput = Mouse.current.delta.ReadValue().FlipY() * rotateRate * Time.unscaledDeltaTime;
			trs.RotateAround(trs.position, trs.right, rotaInput.y);
			trs.RotateAround(trs.position, Vector3.up, rotaInput.x);
		}
	}
}