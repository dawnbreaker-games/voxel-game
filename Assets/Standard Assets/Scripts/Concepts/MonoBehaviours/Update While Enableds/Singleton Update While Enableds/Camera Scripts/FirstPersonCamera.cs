using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace VoxelGame
{
	public class FirstPersonCamera : CameraScript
	{
		public Transform parentTrs;
		public float rotateRate;
		// public float minAngleToLookingUpOrDown;

		public override void HandleRotation ()
		{
			Vector2 rotateInput = Mouse.current.delta.ReadValue().FlipY() * rotateRate * Time.unscaledDeltaTime;
			Vector3 newFacing = Quaternion.Euler(Vector3.up * rotateInput.x + parentTrs.right * rotateInput.y) * parentTrs.forward;
			Quaternion newRotation = Quaternion.LookRotation(newFacing);
			if (Quaternion.Angle(parentTrs.rotation, newRotation) > 160 && rotateInput.magnitude < 160)
			{
				// if (rotateInput.y > 0)
				// 	parentTrs.localEulerAngles = parentTrs.localEulerAngles.SetX(90 - minAngleToLookingUpOrDown);
				// else
				// 	parentTrs.localEulerAngles = parentTrs.localEulerAngles.SetX(-90 + minAngleToLookingUpOrDown);
				// parentTrs.Rotate(Vector3.up * rotateInput.x);
			}
			else
				Player.instance.SetFacing (newFacing);
		}
	}
}