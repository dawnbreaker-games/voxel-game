using DarkRift;
using Extensions;
using UnityEngine;
using DarkRift.Client;
using DarkRift.Client.Unity;
using System.Collections.Generic;

namespace VoxelGame
{
	public class SyncTransform : UpdateWhileEnabled
	{
		public Entity entityIBelongTo;
		public Transform trs;
		public uint id;
		public float syncPositionThreshold;
		public float syncRotationThreshold;
		public _Text idText;
		float syncPositionThresholdSqr;
		Vector3 lastSyncedPosition;
		float lastSyncedRotation;

		void Awake ()
		{
			if (idText != null)
				idText.Text = "" + id;
		}

		public override void OnEnable ()
		{
			base.OnEnable ();
			syncPositionThresholdSqr = syncPositionThreshold * syncPositionThreshold;
			// NetworkManager.syncTransformsDict.Add(id, this);
		}
		
		public override void DoUpdate ()
		{
			if ((trs.position - lastSyncedPosition).sqrMagnitude > syncPositionThresholdSqr)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(id);
					writer.Write(trs.position.x);
					writer.Write(trs.position.y);
					writer.Write(trs.position.z);
					using (Message message = Message.Create(NetworkMessageTags.SYNC_TRANSFORM_MOVED, writer))
						NetworkManager.instance.darkRift2Client.SendMessage(message, SendMode.Unreliable);
				}
				lastSyncedPosition = trs.position;
			}
			if (Mathf.DeltaAngle(trs.eulerAngles.y, lastSyncedRotation) > syncRotationThreshold)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(id);
					writer.Write(trs.eulerAngles.x);
					writer.Write(trs.eulerAngles.y);
					writer.Write(trs.eulerAngles.z);
					using (Message message = Message.Create(NetworkMessageTags.SYNC_TRANSFORM_ROTATED, writer))
						NetworkManager.instance.darkRift2Client.SendMessage(message, SendMode.Unreliable);
				}
				lastSyncedRotation = trs.eulerAngles.y;
			}
		}

		void OnDestroy ()
		{
			NetworkManager.syncTransformsDict.Remove(id);
		}
	}
}