using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace VoxelGame
{
	public class LeaderboardMenu : SingletonMonoBehaviour<LeaderboardMenu>
	{
		public Toggle showAllEntriesToggle;
		public LeaderboardEntry leaderboardEntryPrefab;
		public Transform entriesParent;
		public uint maxEntriesPerPage;
		public _Text pageText;
		public Button openLeaderboardButton;
		public static bool _ShowAllEntries
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show all leaderboard entries", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Show all leaderboard entries", value);
			}
		}
		static LeaderboardEntry.ValueType entryValueType;
		static string[] usernames = new string[0];
		static DatabaseObject dbObj;
		static bool isOpen;
		static bool showAllEntries;
		static uint page;

		public override void Awake ()
		{
			base.Awake ();
			openLeaderboardButton.interactable = !string.IsNullOrEmpty(LocalUserInfo.username);
			showAllEntriesToggle.isOn = _ShowAllEntries;
		}

		void UnloadLeaderboard ()
		{
			isOpen = false;
			for (int i = 0; i < entriesParent.childCount; i ++)
			{
				Transform child = entriesParent.GetChild(i);
				Destroy(child.gameObject);
			}
		}

		void LoadLeaderboard ()
		{
			isOpen = true;
			page = 0;
			pageText.Text = "Page 1 / " + Mathf.Clamp(usernames.Length / maxEntriesPerPage, 1, uint.MaxValue);
			NetworkManager.playerIOClient.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		public void MakeLocalUserAccountInfoPublic (bool makePublic)
		{
			LocalUserInfo.isPublic = makePublic;
		}

		public void ReloadLeaderboard ()
		{
			if (NetworkManager.playerIOClient == null)
			{
				if (string.IsNullOrEmpty(LocalUserInfo.username))
					LocalUserInfo.username = StringExtensions.Random(10, "qwertyuiopasdfghjklzxcvbnm");
				NetworkManager.Connect_PlayerIO (OnConnectSuccess_ReloadLeaderboard, NetworkManager.DisplayError);
				return;
			}
			UnloadLeaderboard ();
			LoadLeaderboard ();
		}

		void OnConnectSuccess_ReloadLeaderboard (Client playerIOClient)
		{
			NetworkManager.playerIOClient = playerIOClient;
			NetworkManager.instance.enabled = true;
			ReloadLeaderboard ();
		}

		public void SetValueType (int valueTypeIndex)
		{
			entryValueType = (LeaderboardEntry.ValueType) Enum.ToObject(typeof(LeaderboardEntry.ValueType), valueTypeIndex);
			ReloadLeaderboard ();
		}

		void OnLoadDBObjectSuccess_LoadLeaderboard (DatabaseObject dbObj)
		{
			DatabaseArray dbArray = dbObj.GetArray("usernames");
			usernames = new string[dbArray.Count];
			for (int i = 0; i < dbArray.Count; i ++)
				usernames[i] = dbArray.GetString(i);
			NetworkManager.playerIOClient.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}
		
		void OnLoadDBObjectsSuccess (DatabaseObject[] dbObjs)
		{
			SortedDictionary<float, List<string>> leaderboardValuesDict = new SortedDictionary<float, List<string>>(new ValueComparer());
			for (int i = 0; i < dbObjs.Length; i ++)
			{
				DatabaseObject dbObj = dbObjs[i];
				float value = 0;
				if (entryValueType == LeaderboardEntry.ValueType.TotalTime)
					value = dbObj.GetFloat("totalTime");
				else if (entryValueType == LeaderboardEntry.ValueType.Tasks)
					value = dbObj.GetUInt("tasksDone");
				List<string> usernamesWithValue = new List<string>();
				if (leaderboardValuesDict.TryGetValue(value, out usernamesWithValue))
					usernamesWithValue.Add(usernames[i]);
				else
					usernamesWithValue = new List<string>(new string[] { usernames[i] });
				leaderboardValuesDict[value] = usernamesWithValue;
			}
			uint rank = 1;
			uint entryIndex = 0;
			foreach (KeyValuePair<float, List<string>> keyValuePair in leaderboardValuesDict)
			{
				for (int i = 0; i < keyValuePair.Value.Count; i ++)
				{
					if (entryIndex >= page * maxEntriesPerPage)
					{
						string username = keyValuePair.Value[i];
						if (showAllEntries || username == LocalUserInfo.username)
						{
							LeaderboardEntry leaderboardEntry = Instantiate(leaderboardEntryPrefab, entriesParent);
							leaderboardEntry.rankText.Text = "" + rank;
							leaderboardEntry.usernameText.Text = username;
							leaderboardEntry.valueText.Text = "" + keyValuePair.Key.ToString("F0");
							leaderboardEntry.valueTypeIndicators[entryValueType.GetHashCode()].SetActive(true);
						}
					}
					entryIndex ++;
					if (entryIndex > page * maxEntriesPerPage + maxEntriesPerPage)
						return;
				}
				rank ++;
				if (rank > maxEntriesPerPage)
					return;
			}
		}

		void OnLoadDBObjectsFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.playerIOClient.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}

		void OnLoadDBObjectFail_LoadLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.playerIOClient.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		public void ShowAllEntries (bool showAllEntries)
		{
			LeaderboardMenu.showAllEntries = showAllEntries;
			ReloadLeaderboard ();
		}

		public void NextLeaderboardPage ()
		{
			if (page < usernames.Length / maxEntriesPerPage)
				page ++;
			else
				page = 0;
			pageText.Text = "Page " + (page + 1) + " / " + Mathf.Clamp(usernames.Length / maxEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		public void PreviousLeaderboardPage ()
		{
			if (page > 0)
				page --;
			else
				page = (uint) (usernames.Length / maxEntriesPerPage);
			pageText.Text = "Page " + (page + 1) + " / " + Mathf.Clamp(usernames.Length / maxEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		class ValueComparer : IComparer<float>
		{
			public int Compare (float value, float value2)
			{
				return MathfExtensions.Sign(value2 - value);
			}
		}
	}
}