using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace VoxelGame
{
	public class MapMenu : Menu, IUpdatable
	{
		public _Text levelNameText;
		public float cameraLerpRate;
		public CameraScript mapCameraScript;
		public Transform currentLevelIndicatorTrs;
#if UNITY_EDITOR
		public bool updateMapCamera;
#endif
		public new static MapMenu instance;
		public new static MapMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MapMenu>(true);
				return instance;
			}
		}
		public static Level targetLevel;
		static CameraUpdater cameraUpdater;
		static Vector2 movementInput;
		static Vector2? previousMovementInput;
		static Vector2? mousePosition;
		static Vector2? previousMousePosition;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (updateMapCamera)
			{
				updateMapCamera = false;
				Level.instances = FindObjectsOfType<Level>();
				Rect[] levelRects = new Rect[Level.instances.Length];
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					levelRects[i] = new Rect((Vector2) level.trs.position - Vector2.one * level.size / 2, Vector2.one * level.size);
				}
				Rect mapRect = levelRects.Combine();
				mapCameraScript.trs.position = mapRect.center.SetZ(mapCameraScript.trs.position.z);
				mapCameraScript.viewSize = mapRect.size;
			}
		}
#endif

		public override void Open ()
		{
			base.Open ();
			previousMovementInput = null;
			List<Level> levels = new List<Level>(FindObjectsOfType<Level>());
			for (int i = 0; i < levels.Count; i ++)
			{
				Level level = levels[i];
				if (!level.gameObject.activeSelf)
				{
					levels.RemoveAt(i);
					i --;
				}
				else if (level.BestTimeReached > 0)
					level.untriedIndicatorGo.SetActive(false);
			}
			Level.instances = levels.ToArray();
			SetTargetLevel (Level.instance);
			CameraScript.Instance.trs.position = targetLevel.trs.position.SetZ(CameraScript.instance.trs.position.z);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void Close ()
		{
			base.Close ();
			CameraScript.Instance.trs.position = Level.instance.trs.position.SetZ(CameraScript.instance.trs.position.z);
			targetLevel.bestTimeReachedText.gameObject.SetActive(false);
		}

		public void DoUpdate ()
		{
			movementInput = InputManager.MovementInput;
			mousePosition = InputManager.MousePosition;
			HandleSetTargetLevel ();
			if (Level.instance.unlocked && !EventSystem.current.IsPointerOverGameObject() && (Keyboard.current.enterKey.wasPressedThisFrame || Keyboard.current.spaceKey.wasPressedThisFrame || Mouse.current.leftButton.wasPressedThisFrame))
			{
				Close ();
				if (!Level.instance.enabled)
				{
					Player.instance.trs.position = Level.instance.playerSpawnPoint.position;
					Player.instance.trs.SetParent(null);
					Level.instance.moveToBeginTextGo.SetActive(true);
					GameManager.updatables = GameManager.updatables.Add(new BeginLevelUpdater());
				}
				else if (targetLevel != Level.instance)
				{
					Level previousLevel = Level.instance;
					Level.instance = targetLevel;
					previousLevel.End ();
				}
			}
			previousMovementInput = movementInput;
			previousMousePosition = mousePosition;
		}

		void HandleSetTargetLevel ()
		{
			Level newTargetLevel = null;
			if (previousMovementInput == Vector2.zero && movementInput != Vector2.zero)
			{
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					Vector2 toLevel = level.trs.position - targetLevel.trs.position;
					float manhattenDistance = toLevel.GetManhattenMagnitude();
					if (manhattenDistance == level.size && Vector2.Angle(toLevel, movementInput) < 45)
					{
						newTargetLevel = level;
						break;
					}
				}
				if (newTargetLevel == null)
				{
					for (int i = 0; i < Level.instances.Length; i ++)
					{
						Level level = Level.instances[i];
						Vector2 toLevel = level.trs.position - targetLevel.trs.position;
						float manhattenDistance = toLevel.GetManhattenMagnitude();
						if (manhattenDistance == level.size * 2 && Vector2.Angle(toLevel, movementInput) < 45)
						{
							newTargetLevel = level;
							break;
						}
					}
					if (newTargetLevel == null)
						newTargetLevel = GetLevelAtWrappedMapMovementInput(movementInput);
				}
			}
			else if (mousePosition != null && mousePosition != previousMousePosition)
			{
				RenderTexture previousRenderTexture = mapCameraScript.camera.targetTexture;
				mapCameraScript.camera.targetTexture = null;
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					if (new Rect((Vector2) level.trs.position - Vector2.one * level.innerSize / 2, Vector2.one * level.innerSize).Contains(mapCameraScript.camera.ScreenToWorldPoint((Vector2) mousePosition)))
					{
						newTargetLevel = level;
						break;
					}
				}
				mapCameraScript.camera.targetTexture = previousRenderTexture;
			}
			if (newTargetLevel != null && newTargetLevel != targetLevel)
				SetTargetLevel (newTargetLevel);
		}

		void SetTargetLevel (Level level)
		{
			if (!Level.instance.enabled)
			{
				Level.instance = level;
				if (level is BossLevel)
					level.bestTimeReachedText.Text = "Best damage: " + string.Format("{0:0.#}", level.BestTimeReached);
				else
					level.bestTimeReachedText.Text = "Best time: " + string.Format("{0:0.#}", level.BestTimeReached);
			}
			targetLevel = level;
			currentLevelIndicatorTrs.position = level.trs.position;
			levelNameText.Text = level.displayName;
			if (cameraUpdater != null)
				GameManager.updatables = GameManager.updatables.Remove(cameraUpdater);
			cameraUpdater = new CameraUpdater(level.trs.position, cameraLerpRate);
			GameManager.updatables = GameManager.updatables.Add(cameraUpdater);
			AchievementPanel.Instance.DoUpdate ();
		}

		static Level GetLevelAtWrappedMapMovementInput (Vector2 input)
		{
			Level output = null;
			float closestManhattenDistance = 0;
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				Vector2 toLevel = level.trs.position - targetLevel.trs.position;
				float manhattenDistance = toLevel.GetManhattenMagnitude();
				if ((toLevel.x == 0 || toLevel.y == 0) && manhattenDistance > closestManhattenDistance && Vector2.Angle(toLevel, input) > 135)
				{
					output = level;
					closestManhattenDistance = manhattenDistance;
				}
			}
			return output;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(cameraUpdater);
		}

		class CameraUpdater : IUpdatable
		{
			Vector2 destination;
			float lerpRate;

			public CameraUpdater (Vector2 destination, float lerpRate)
			{
				this.destination = destination;
				this.lerpRate = lerpRate;
			}

			public void DoUpdate ()
			{
				float levelDistance = ((Vector2) CameraScript.instance.trs.position - destination).magnitude;
				float moveAmount = levelDistance * lerpRate * Time.deltaTime;
				if (levelDistance > moveAmount)
					CameraScript.instance.trs.position = Vector2.Lerp(CameraScript.instance.trs.position.SetZ(0), destination, lerpRate * Time.deltaTime).SetZ(CameraScript.instance.trs.position.z);
				else
				{
					CameraScript.instance.trs.position = destination.SetZ(CameraScript.instance.trs.position.z);
					cameraUpdater = null;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		class BeginLevelUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				if (InputManager.MovementInput != Vector2.zero)
				{
					Level.instance.Begin ();
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}