using Extensions;
using UnityEngine;

namespace VoxelGame
{
	public class AchievementPanel : SingletonMonoBehaviour<AchievementPanel>
	{
		public Transform achievementIndicatorsParent;
		public AchievementIndicator achievementIndicatorPrefab;

		public void DoUpdate ()
		{
			GameManager.instance.DestroyChildren (achievementIndicatorsParent);
			Achievement[] achievements = MapMenu.targetLevel.GetComponentsInChildren<Achievement>();
			for (int i = 0; i < achievements.Length; i ++)
			{
				Achievement achievement = achievements[i];
				AchievementIndicator achievementIndicator = Instantiate(achievementIndicatorPrefab, achievementIndicatorsParent);
				achievementIndicator.trs.localScale = Vector3.one;
				string text;
				if (achievement.unlockLevelOnComplete.trs.position.x > MapMenu.targetLevel.trs.position.x)
					text = ">";
				else if (achievement.unlockLevelOnComplete.trs.position.x < MapMenu.targetLevel.trs.position.x)
					text = "<";
				else if (achievement.unlockLevelOnComplete.trs.position.y > MapMenu.targetLevel.trs.position.y)
					text = "^";
				else
					text = "v";
				if (achievement.complete)
					text += " (Done)";
				text += " " + achievement.displayName;
				if (!achievement.complete)
					text += " (" + achievement.GetProgress() + "/" + achievement.GetMaxProgress() + ")";
				achievementIndicator.text.Text = text;
			}
		}
	}
}