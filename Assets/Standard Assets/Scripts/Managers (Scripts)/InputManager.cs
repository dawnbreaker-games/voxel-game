﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace VoxelGame
{
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		public InputDevice inputDevice;
		public InputSettings settings;
		public float minPressMagnitude;
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.leftButton.isPressed;
			}
		}
		public static bool RightClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.rightButton.isPressed;
			}
		}
		public static bool RestartInput
		{
			get
			{
				return UsingKeyboard && Keyboard.current.rKey.isPressed;
			}
		}
		public static bool MenuInput
		{
			get
			{
				return UsingKeyboard && Keyboard.current.escapeKey.isPressed;
			}
		}
		public static int SwitchMenuInput
		{
			get
			{
				int output = 0;
				if (UsingKeyboard)
				{
					if (Keyboard.current.qKey.isPressed)
						output --;
					if (Keyboard.current.eKey.isPressed)
						output ++;
				}
				return output;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return null;
			}
		}
		public static bool JumpInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else if (UsingKeyboard)
					return Keyboard.current.spaceKey.isPressed;
				else
					return false;
			}
		}
		public static int? SwitchItemInput
		{
			get
			{
				if (Keyboard.current.digit1Key.isPressed)
					return 0;
				else if (Keyboard.current.digit2Key.isPressed)
					return 1;
				else if (Keyboard.current.digit3Key.isPressed)
					return 2;
				else if (Keyboard.current.digit4Key.isPressed)
					return 3;
				else if (Keyboard.current.digit5Key.isPressed)
					return 4;
				else if (Keyboard.current.digit6Key.isPressed)
					return 5;
				else if (Keyboard.current.digit7Key.isPressed)
					return 6;
				else if (Keyboard.current.digit8Key.isPressed)
					return 7;
				else if (Keyboard.current.digit9Key.isPressed)
					return 8;
				else if (Keyboard.current.digit0Key.isPressed)
					return 9;
				else
					return null;
			}
		}
		public static bool UseItemInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightTrigger.isPressed;
				else if (UsingMouse)
					return Mouse.current.leftButton.isPressed;
				else
					return false;
			}
		}
		public static bool SubmitInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else if (UsingKeyboard)
					return Keyboard.current.enterKey.isPressed;
				else
					return false;
			}
		}
		public static Vector2 MovementInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ReadValue(), 1);
				else
				{
					int x = 0;
					if (Keyboard.current.dKey.isPressed)
						x ++;
					if (Keyboard.current.aKey.isPressed)
						x --;
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(x, y), 1);
				}
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			Phone,
			Gamepad
		}
	}
}