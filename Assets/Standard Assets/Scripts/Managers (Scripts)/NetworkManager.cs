using System;
using DarkRift;
using System.Net;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using DarkRift.Client;
using System.Net.Sockets;
using System.Collections;
using System.Diagnostics;
using DarkRift.Client.Unity;
using System.Collections.Generic;
using Message = DarkRift.Message;

namespace VoxelGame
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public UnityClient darkRift2Client;
		public Rect entitySpatialHashGridRect;
		public float entitySpatialHashGridCellSize;
		// public TMP_Text playerListText;
		// public TMP_Text currentPlayerText;
		// public NotificationText notificationTextPrefab;
		// public Transform notificationTextsParent;
		public static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
		public static Dictionary<uint, SyncTransform> syncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static Dictionary<uint, SyncTransform> npcSyncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static SpatialHashGrid2D<Entity> entitySpatialHashGrid;
		public static Dictionary<Entity, SpatialHashGrid2D<Entity>.Agent> entitySpatialHashGridAgentsDict = new Dictionary<Entity, SpatialHashGrid2D<Entity>.Agent>();
		public static Connection connection;
		public static Client playerIOClient;
		static DatabaseObject dbObj;

		void Start ()
		{
			SyncTransform[] syncTransforms = FindObjectsOfType<SyncTransform>();
			for (int i = 0; i < syncTransforms.Length; i ++)
			{
				SyncTransform syncTrs = syncTransforms[i];
				npcSyncTransformsDict.Add(syncTrs.id, syncTrs);
			}
			entitySpatialHashGrid = new SpatialHashGrid2D<Entity>(entitySpatialHashGridRect, entitySpatialHashGridCellSize);
			Connect_DarkRift2 ();
		}

		public static void Connect_PlayerIO (Callback<Client> onSuccess, Callback<PlayerIOError> onFail)
		{
			PlayerIO.UseSecureApiRequests = true;
			PlayerIO.Authenticate("voxel-game-fflp0zefj0ib2bysujlaug",
				"public",
				new Dictionary<string, string> {
					{ "userId", LocalUserInfo.username },
				},
				null,
				onSuccess,
				onFail
			);
		}

		public void Connect_DarkRift2 ()
		{
			darkRift2Client.MessageReceived -= OnMessageReceived;
			darkRift2Client.MessageReceived += OnMessageReceived;
			darkRift2Client.ConnectInBackground(GetLocalIPAddress(), darkRift2Client.Port, false, OnConnectDone);
		}

		void OnConnectDone (Exception e)
		{
			if (e != null)
			{
				print(e.Message + "\n" + e.StackTrace);
				Connect_DarkRift2 ();
			}
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				if (message.Tag == NetworkMessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (eventArgs);
				else if (message.Tag == NetworkMessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransformMoved (eventArgs);
				else if (message.Tag == NetworkMessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (eventArgs);
				else if (message.Tag == NetworkMessageTags.PLAYER_LEFT)
					OnPlayerLeft (eventArgs);
			}
		}

		void OnSpawnPlayer (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						Quaternion rotation;
						if (reader.Length == 20)
							rotation = Quaternion.Euler(Vector3.up * reader.ReadSingle());
						else
							rotation = Quaternion.Euler(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						if (!playersDict.ContainsKey(id))
							SpawnPlayer (position, rotation, id);
					}
				}
			}
		}

		void OnSyncTransformMoved (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
							syncTrs.trs.position = position;
					}
				}
			}
		}

		void OnSyncTransformRotated (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Vector3 rotation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
						{
							syncTrs.trs.eulerAngles = rotation;
							if (syncTrs.entityIBelongTo != null)
								syncTrs.entityIBelongTo.SetFacing (Quaternion.Euler(rotation) * Vector3.forward);
						}
					}
				}
			}
		}

		void OnPlayerLeft (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Player player;
						if (playersDict.TryGetValue(id, out player))
						{
							Destroy(player.gameObject);
							entitySpatialHashGridAgentsDict.Remove(player);
							// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
							// notificationText.text.text = "Player " + id + " left";
							playersDict.Remove(id);
							syncTransformsDict.Remove(id);
							// UpdatePlayerListText ();
						}
					}
				}
			}
		}

		public Player SpawnPlayer (Vector3 position, Quaternion rotation, uint id)
		{
			Player output = Instantiate(GameMode.instance.playerPrefab, position, rotation);
			output.Id = id;
			// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
			// notificationText.text.text = "Player " + id + " joined";
			if (output.syncTrs.idText != null)
				output.syncTrs.idText.Text = "" + id;
			playersDict.Add(id, output);
			syncTransformsDict.Add(id, output.syncTrs);
			// UpdatePlayerListText ();
			if (id == darkRift2Client.ID)
			// {
				output.enabled = true;
			// 	currentPlayerText.text = "You are: " + id;
			// }
			// entitySpatialHashGridAgentsDict.Add(output, new SpatialHashGrid2D<Entity>.Agent(position.GetXZ(), output, entitySpatialHashGrid));
			return output;
		}

		// void UpdatePlayerListText ()
		// {
		// 	playerListText.text = "Players: ";
		// 	foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
		// 		playerListText.text += keyValuePair.Key + ", ";
		// }

		public static void Disconnect ()
		{
			if (connection != null)
				connection.Disconnect();
			if (playerIOClient != null)
				playerIOClient.Logout();
		}

		public static void DisplayError (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification (error.ToString());
		}

		public static void DisplayErrorAndRetry (PlayerIOError error)
		{
			DisplayError (error);
			new StackTrace().GetFrame(1).GetMethod().Invoke(null, null);
		}

		public static void PrintError (PlayerIOError error)
		{
			print(error.ToString());
		}
		
		public static string GetLocalIPAddress ()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == AddressFamily.InterNetwork)
					return ip.ToString();
			}
			throw new System.Exception("No network adapters with an IPv4 address in the system!");
		}
	}
}