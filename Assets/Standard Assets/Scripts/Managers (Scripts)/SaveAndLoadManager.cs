﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace VoxelGame
{
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public static SaveData saveData = new SaveData();
		public static string MostRecentSaveFileName
		{
			get
			{
				return PlayerPrefs.GetString("Most recent save file name", null);
			}
			set
			{
				PlayerPrefs.SetString("Most recent save file name", value);
			}
		}
		public static string filePath;
		
		void Start ()
		{
			filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Auto-Save";
		}

		static void OnAboutToSave ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			List<string> completeAchievementsNames = new List<string>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (achievement.complete)
					completeAchievementsNames.Add(achievement.name);
			}
			saveData.completeAchievementsNames = completeAchievementsNames.ToArray();
			saveData.levelName = Level.instance.name;
			saveData.playerName = Player.instance.name;
		}
		
		public static void Save (string fileName)
		{
			OnAboutToSave ();
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
			MostRecentSaveFileName = fileName;
		}
		
		public void Load (string fileName)
		{
			print(fileName);
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			OnLoad (fileName);
		}

		void OnLoad (string fileName)
		{
			MostRecentSaveFileName = fileName;
			OnLoaded ();
		}

		void OnLoaded ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				achievement.Init ();
			}
			Level.instances = FindObjectsOfType<Level>();
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level.name == saveData.levelName)
				{
					Level.instance = level;
					break;
				}
			}
		}
		
		public void LoadMostRecent ()
		{
			Load (MostRecentSaveFileName);
		}
		
		[Serializable]
		public struct SaveData
		{
			public string[] completeAchievementsNames;
			public string levelName;
			public string playerName;
			public Dictionary<string, float> bestLevelTimesDict;
			public string[] triedPlayers;
			public bool showTutorials;

		}
	}
}