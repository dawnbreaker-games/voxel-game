using System;
using VoxelGame;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class DatabaseExtensions
	{
		public static int IndexOf (this DatabaseArray dbArray, string find)
		{
			for (int i = 0; i < dbArray.Count; i ++)
			{
				object value = dbArray.GetValue(i);
				if (value == find)
					return i;
			}
			return -1; 
		}
		
		public static T GetOrMakeValue<T> (this DatabaseObject dbObj, string valueName, T defaultValue = default(T))
		{
			object value = dbObj.GetValue(valueName);
			if (value == null)
			{
				dbObj.SetValue (valueName, defaultValue);
				return defaultValue;
			}
			else
				return (T) value;
		}
		
		public static object GetOrMakeValue (this DatabaseObject dbObj, string valueName, object defaultValue)
		{
			object value = dbObj.GetValue(valueName);
			if (value == null)
			{
				dbObj.SetValue (valueName, defaultValue);
				return defaultValue;
			}
			else
				return value;
		}

		public static void SetValue (this DatabaseObject dbObj, string valueName, object value)
		{
			if (value is uint)
				dbObj.Set(valueName, (uint) value);
			else if (value is int)
				dbObj.Set(valueName, (int) value);
			else if (value is long)
				dbObj.Set(valueName, (long) value);
			else if (value is float)
				dbObj.Set(valueName, (float) value);
			else if (value is double)
				dbObj.Set(valueName, (double) value);
			else if (value is string)
				dbObj.Set(valueName, (string) value);
			else if (value is DatabaseArray)
				dbObj.Set(valueName, (DatabaseArray) value);
			else// if (value is DatabaseObject)
				dbObj.Set(valueName, (DatabaseObject) value);
		}
	}
}