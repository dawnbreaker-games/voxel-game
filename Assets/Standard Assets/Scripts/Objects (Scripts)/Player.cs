using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace VoxelGame
{
	public class Player : Entity
	{
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public uint Id
		{
			get
			{
				return syncTrs.id;
			}
			set
			{
				syncTrs.id = value;
			}
		}
		public GameObject cameraGo;
		public Transform itemsParent;
		[HideInInspector]
		public Item[] items = new Item[0];
		[HideInInspector]
		public UsableItem[] usableItems = new UsableItem[0];
		public Action[] actions = new Action[0];
		public SortedList<string, BulletPatternEntry> bulletPatternEntriesSortedList = new SortedList<string, BulletPatternEntry>();
		public Transform bulletSpawnersParent;
		static GameMode.RespawnUpdater respawnUpdater;
		Dictionary<string, Action> actionsDict = new Dictionary<string, Action>();
		UsableItem currentUsableItem;
		bool useItemInput;
		bool previousUseItemInput;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instance = this;
			cameraGo.SetActive(true);
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				item.OnGain ();
			}
			if (usableItems.Length > 0)
				currentUsableItem = usableItems[0];
			for (int i = 0; i < actions.Length; i ++)
			{
				Action action = actions[i];
				actionsDict.Add(action.name, action);
			}
			base.OnEnable ();
		}

		void OnValidate ()
		{
			items = itemsParent.GetComponentsInChildren<Item>();
			usableItems = itemsParent.GetComponentsInChildren<UsableItem>();
		}
		
		public override void DoUpdate ()
		{
			useItemInput = InputManager.UseItemInput;
			base.DoUpdate ();
			// HandleItemSwitching ();
			// HandleUseItem ();
			// foreach (KeyValuePair<uint, SyncTransform> keyValuePair in NetworkManager.syncTransformsDict)
			// 	keyValuePair.Value.canvasTrs.forward = GameCamera.instance.trs.position - keyValuePair.Value.canvasTrs.position;
			previousUseItemInput = useItemInput;
		}

		public override void HandleRotating ()
		{
		}

		Vector3 GetMoveInput ()
		{
			Vector3 moveInput = InputManager.MovementInput;
			moveInput = moveInput.XYToXZ();
			moveInput = trs.rotation * moveInput;
			return moveInput;
		}
		
		public override void HandleMoving ()
		{
			move = GetMoveInput();
			base.HandleMoving ();
		}
		
		public override void HandleJump ()
		{
			if (canJump && InputManager.JumpInput && Time.time - timeLastGrounded < maxJumpDuration)
			{
				if (isGrounded)
					yVel = 0;
				Jump ();
			}
			else
			{
				if (yVel > 0)
					yVel = 0;
				canJump = false;
			}
		}

		public override void Death ()
		{
			if (isDead)
				return;
			isDead = true;
			NetworkManager.entitySpatialHashGridAgentsDict[this].Remove ();
			NetworkManager.playersDict.Remove(Id);
			gameObject.SetActive(false);
			respawnUpdater = new GameMode.RespawnUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(respawnUpdater);
		}

		void HandleItemSwitching ()
		{
			if (InputManager.SwitchItemInput != null && usableItems.Length > (int) InputManager.SwitchItemInput)
				SwitchToItem (usableItems[(int) InputManager.SwitchItemInput]);
		}

		void SwitchToItem (UsableItem usableItem)
		{
			if (currentUsableItem == usableItem)
				return;
			if (currentUsableItem != null)
				currentUsableItem.Unequip ();
			usableItem.Equip ();
			currentUsableItem = usableItem;
		}

		void HandleUseItem ()
		{
			if (useItemInput && !previousUseItemInput)
				currentUsableItem.Use ();
		}

		public void DoAction (Action action, float startTime)
		{
			animator.Play(action.animStateName, action.animStateLayerIndex, ((float) GameManager.GetTimeInSeconds() - startTime) / action.duration);
		}

		[Serializable]
		public struct Action
		{
			public string name;
			public string animStateName;
			public int animStateLayerIndex;
			public float duration;
		}
	}
}