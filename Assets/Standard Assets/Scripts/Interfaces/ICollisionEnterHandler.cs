using UnityEngine;

namespace VoxelGame
{
	public interface ICollisionEnterHandler
	{
        Collider Collider { get; }
        
        void OnCollisionEnter (Collision coll);
	}
}