using UnityEngine;

namespace VoxelGame
{
	public interface ICollisionExitHandler
	{
        Collider Collider { get; }
        
        void OnCollisionExit (Collision coll);
	}
}