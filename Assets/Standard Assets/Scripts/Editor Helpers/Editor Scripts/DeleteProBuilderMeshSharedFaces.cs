#if PROBUILDER
using UnityEngine;
using System.Collections;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using UnityEngine.ProBuilder.MeshOperations;
#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif

namespace AmbitiousSnake
{
	public class DeleteProBuilderMeshSharedFaces
	{
		public static void Do (ProBuilderMesh proBuilderMesh)
		{
#if UNITY_EDITOR
			EditorCoroutineUtility.StartCoroutine(DoRoutine (proBuilderMesh), Object.FindObjectOfType<MonoBehaviour>());
#else
			Object.FindObjectOfType<MonoBehaviour>().StartCoroutine(DoRoutine (proBuilderMesh));
#endif
		}

		static IEnumerator DoRoutine (ProBuilderMesh proBuilderMesh)
		{
			MonoBehaviour.print("DeleteProBuilderMeshSharedFaces operation began for " + proBuilderMesh.name);
			while (true)
			{
				bool removedAFace = false;
				for (int i = 0; i < proBuilderMesh.faceCount; i ++)
				{
					Face face = proBuilderMesh.faces[i];
					for (int i2 = i + 1; i2 < proBuilderMesh.faceCount; i2 ++)
					{
						Face face2 = proBuilderMesh.faces[i2];
						bool isSharedFace = true;
						for (int i3 = 0; i3 < face.indexes.Count; i3 ++)
						{
							int index = face.indexes[i3];
							bool isSharedIndex = false;
							for (int i4 = 0; i4 < face2.indexes.Count; i4 ++)
							{
								yield return new WaitForEndOfFrame();
								int index2 = face2.indexes[i4];
								if (proBuilderMesh.positions[index] == proBuilderMesh.positions[index2])
								{
									isSharedIndex = true;
									break;
								}
							}
							if (!isSharedIndex)
							{
								isSharedFace = false;
								break;
							}
						}
						if (isSharedFace)
						{
							proBuilderMesh.DeleteFaces(new int[2] { i, i2 });
							proBuilderMesh.ToMesh();
							proBuilderMesh.Refresh();
							MonoBehaviour.print("DeleteProBuilderMeshSharedFaces operation for " + proBuilderMesh.name + " removed a face");
							removedAFace = true;
						}
					}
				}
				if (!removedAFace)
					break;
				MonoBehaviour.print("DeleteProBuilderMeshSharedFaces operation continues for " + proBuilderMesh.name);
			}
			MonoBehaviour.print("DeleteProBuilderMeshSharedFaces operation done for " + proBuilderMesh.name);
		}

#if UNITY_EDITOR
		[MenuItem("Tools/Delete shared faces of selected ProBuilderMeshes")]
		static void _Do ()
		{
			ProBuilderMesh[] proBuilderMeshes = SelectionExtensions.GetSelected<ProBuilderMesh>();
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				Do (proBuilderMesh);
			}
		}
#endif
	}
}
#endif