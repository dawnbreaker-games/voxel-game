#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	[ExecuteInEditMode]
	public class SetAxisAngleRotation : EditorScript
	{
		public Transform trs;
		public Vector3 axis;
		public float angle;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.rotation = Quaternion.AxisAngle(axis, angle);
		}
	}
}
#else
namespace VoxelGame
{
	public class SetAxisAngleRotation : EditorScript
	{
	}
}
#endif