#if PROBUILDER
using UnityEngine;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using UnityEngine.ProBuilder.MeshOperations;
using Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AmbitiousSnake
{
	public class DeleteProBuilderMeshFacesExceptFacesOfBounds
	{
		public static void Do (ProBuilderMesh proBuilderMesh)
		{
			Bounds bounds = proBuilderMesh.GetComponent<MeshRenderer>().bounds;
			Transform trs = proBuilderMesh.GetComponent<Transform>();
			while (true)
			{
				bool removedAFace = false;
				for (int i = 0; i < proBuilderMesh.faceCount; i ++)
				{
					Face face = proBuilderMesh.faces[i];
					Plane[] facePlanes = bounds.GetOutsideFacePlanes();
					bool containsAllPoints = true;
					for (int i2 = 0; i2 < facePlanes.Length; i2 ++)
					{
						containsAllPoints = true;
						Plane facePlane = facePlanes[i2];
						for (int i3 = 0; i3 < face.indexes.Count; i3 ++)
						{
							int index = face.indexes[i3];
							if (Mathf.Abs(facePlane.GetDistanceToPoint(trs.TransformPoint(proBuilderMesh.positions[index]))) >= 0.1f)
							{
								containsAllPoints = false;
								break;
							}
						}
						if (containsAllPoints)
							break;
					}
					if (!containsAllPoints)
					{
						proBuilderMesh.DeleteFaces(new int[1] { i });
						removedAFace = true;
						break;
					}
				}
				if (!removedAFace)
					break;
			}
			proBuilderMesh.ToMesh();
			proBuilderMesh.Refresh();
		}

#if UNITY_EDITOR
		[MenuItem("Tools/Delete faces of selected ProBuilderMeshes except faces of bounds")]
		static void _Do ()
		{
			ProBuilderMesh[] proBuilderMeshes = SelectionExtensions.GetSelected<ProBuilderMesh>();
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				Do (proBuilderMesh);
			}
		}
#endif
	}
}
#endif