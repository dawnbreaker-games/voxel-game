#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class DrawCapsuleGizmo : MonoBehaviour
{
	public CharacterController characterController;
	public Mesh capsuleMesh;

	void OnValidate ()
	{
		if (characterController == null)
			characterController = GetComponent<CharacterController>();
	}

	[DrawGizmo(GizmoType.InSelectionHierarchy)]
	static void DrawGizmo (DrawCapsuleGizmo drawCapsuleGizmo, GizmoType gizmoType)
	{
		CharacterController characterController = drawCapsuleGizmo.characterController;
		Transform trs = characterController.GetComponent<Transform>();
		Gizmos.DrawMesh(drawCapsuleGizmo.capsuleMesh, trs.position, trs.rotation, trs.lossyScale);
	}
}
#endif