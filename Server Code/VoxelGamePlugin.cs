using System;
using DarkRift;
using DarkRift.Server;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

namespace VoxelGame
{
	public class VoxelGamePlugin : Plugin
	{
		public override Version Version => new Version(1, 0, 0);
		public override bool ThreadSafe => true;
		public static VoxelGamePlugin instance;
		public static Random random = new Random();
		public static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
		public static Dictionary<uint, SyncTransform> syncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static SpawnPoint[] spawnPoints = new SpawnPoint[] {
			new SpawnPoint(Orientation3D.Identity)
		};

		public VoxelGamePlugin (PluginLoadData pluginLoadData) : base (pluginLoadData)
		{
			instance = this;
			ClientManager.ClientConnected += OnClientConnected;
			ClientManager.ClientDisconnected += OnClientDisconnected;
		}

		void OnClientConnected (object sender, ClientConnectedEventArgs eventArgs)
		{
			byte spawnPointIndex = (byte) MathExtensions.Random(0, spawnPoints.Length, random);
			SpawnPoint spawnPoint = spawnPoints[spawnPointIndex];
			SyncTransform syncTrs = new SyncTransform(spawnPoint.orientation, eventArgs.Client.ID);
			Player player = new Player(syncTrs);
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(syncTrs.id);
				writer.Write(spawnPoint.orientation.position.x);
				writer.Write(spawnPoint.orientation.position.y);
				writer.Write(spawnPoint.orientation.position.z);
				writer.Write(spawnPoint.orientation.rotation.y);
				using (Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				foreach (Player previousPlayer in playersDict.Values)
				{
					writer.Write(previousPlayer.syncTrs.id);
					writer.Write(previousPlayer.syncTrs.orientation.position.x);
					writer.Write(previousPlayer.syncTrs.orientation.position.y);
					writer.Write(previousPlayer.syncTrs.orientation.position.z);
					writer.Write(previousPlayer.syncTrs.orientation.rotation.x);
					writer.Write(previousPlayer.syncTrs.orientation.rotation.y);
					writer.Write(previousPlayer.syncTrs.orientation.rotation.z);
					using (Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			foreach (SyncTransform syncTrs2 in syncTransformsDict.Values)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(syncTrs2.id);
					writer.Write(syncTrs2.orientation.position.x);
					writer.Write(syncTrs2.orientation.position.y);
					writer.Write(syncTrs2.orientation.position.z);
					using (Message message = Message.Create(NetworkMessageTags.SYNC_TRANSFORM_MOVED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(syncTrs2.id);
					writer.Write(syncTrs2.orientation.rotation.x);
					writer.Write(syncTrs2.orientation.rotation.y);
					writer.Write(syncTrs2.orientation.rotation.z);
					using (Message message = Message.Create(NetworkMessageTags.SYNC_TRANSFORM_ROTATED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			playersDict.Add(syncTrs.id, player);
			syncTransformsDict.Add(syncTrs.id, syncTrs);
			eventArgs.Client.MessageReceived += OnMessageReceived;
		}

		void OnClientDisconnected (object sender, ClientDisconnectedEventArgs eventArgs)
		{
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(eventArgs.Client.ID);
				using (Message message = Message.Create(NetworkMessageTags.PLAYER_LEFT, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			playersDict.Remove(eventArgs.Client.ID);
			syncTransformsDict.Remove(eventArgs.Client.ID);
			eventArgs.Client.MessageReceived -= OnMessageReceived;
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == NetworkMessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransformMoved (eventArgs);
				else if (message.Tag == NetworkMessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (eventArgs);
			}
		}

		void OnSyncTransformMoved (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs = syncTransformsDict[reader.ReadUInt32()];
					syncTrs.orientation.position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, eventArgs.SendMode);
					}
				}
			}
		}

		void OnSyncTransformRotated (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs = syncTransformsDict[reader.ReadUInt32()];
					syncTrs.orientation.rotation = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, eventArgs.SendMode);
					}
				}
			}
		}
	}
}