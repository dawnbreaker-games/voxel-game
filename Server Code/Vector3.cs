using System;

public struct Vector3
{
	public float x;
	public float y;
	public float z;
	public float Magnitude
	{
		get
		{
			return (float) Math.Sqrt(x * x + y * y + z * z);
		}
	}
	public float SqrMagnitude
	{
		get
		{
			return x * x + y * y + z * z;
		}
	}
	public static Vector3 Up
	{
		get
		{
			return new Vector3(0, 1, 0);
		}
	}
	public static Vector3 One
	{
		get
		{
			return new Vector3(1, 1, 1);
		}
	}

	public Vector3 (float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3 Normalized ()
	{
		return this / Magnitude;
	}

	public static Vector3 FromNormalizedPoint (Vector3 center, Vector3 size, Vector3 normalizedPoint)
	{
		return center + size * normalizedPoint;
	}

	public static Vector3 ToNormalizedPosition (Vector3 center, Vector3 size, Vector3 point)
	{
		return One / size * (point - center);
	}

	public static Vector3 operator+ (Vector3 v, Vector3 v2)
	{
		return new Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
	}

	public static Vector3 operator- (Vector3 v, Vector3 v2)
	{
		return new Vector3(v.x - v2.x, v.y - v2.y, v.z - v2.z);
	}

	public static Vector3 operator* (Vector3 v, Vector3 v2)
	{
		return new Vector3(v.x * v2.x, v.y * v2.y, v.z * v2.z);
	}

	public static Vector3 operator/ (Vector3 v, Vector3 v2)
	{
		return new Vector3(v.x / v2.x, v.y / v2.y, v.z / v2.z);
	}

	public static Vector3 operator* (Vector3 v, float f)
	{
		return new Vector3(v.x * f, v.y * f, v.z * f);
	}

	public static Vector3 operator/ (Vector3 v, float f)
	{
		return new Vector3(v.x / f, v.y / f, v.z / f);
	}
}