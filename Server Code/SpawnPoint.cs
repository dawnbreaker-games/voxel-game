namespace VoxelGame
{
	public class SpawnPoint
	{
        public Orientation3D orientation;

		public SpawnPoint (Orientation3D orientation)
		{
			this.orientation = orientation;
		}
	}
}