public struct Orientation3D
{
	public Vector3 position;
	public Vector3 rotation;
	public static Orientation3D Identity
	{
		get
		{
			return new Orientation3D();
		}
	}

	public Orientation3D (Vector3 position, Vector3 rotation)
	{
		this.position = position;
		this.rotation = rotation;
	}
}