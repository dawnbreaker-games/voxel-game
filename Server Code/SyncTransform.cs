public struct SyncTransform
{
	public Orientation3D orientation;
    public uint id;

	public SyncTransform (Orientation3D orientation, uint id)
	{
		this.orientation = orientation;
        this.id = id;
	}
}